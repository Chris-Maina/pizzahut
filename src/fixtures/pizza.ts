export const pizzaNames = [
  'CHEESY CHICKEN, CHORIZO & BACON',
  'BBQ PORK & ONION',
  'PEPPERONI',
  'SIMPLY CHEESE',
  'CHEESY GARLIC PIZZA',
  'HAWAIIAN',
  'VEG TRIO',
  'BEEF & ONION'
];

export const pizzaSizes = [
  'Small',
  'Medium',
  'Large',
  'Small',
  'Medium',
  'Large',
  'Small',
  'Medium'
];

export const pizzaPrices = [
  '500',
  '800',
  '1000',
  '1200',
  '500',
  '800',
  '1000',
  '1200'
];

export const samplePizza = {
  description: 'Spanish style Chorizo, red onion on a tomato base with lots of stretchy mozzarella cheese',
  name: 'CHEESY CHICKEN, CHORIZO & BACON',
  price: 500,
  size: 'Small'
};

export const samplePizzas = Array(8).fill({}).map((value, index) => {
  return {
    description: 'This is a pizza description',
    name: pizzaNames[index],
    price: pizzaPrices[index],
    size: pizzaSizes[index]
  };
});
