// React lib import
import * as React from 'react';

// third-party libraries
import { Button } from 'semantic-ui-react';

export const PizzaButton = (props: any): any => {
  return(
    <Button
     color={props.color} 
     content={props.label}
    />
  ); 
};