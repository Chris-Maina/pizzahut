// React lib import
import * as React from 'react';

// third-party libraries
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// components
import { HomePage } from './components/HomePage/HomePage.component';
import PizzaPage from './components/PizzaPage/PizzaPage.component';

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={HomePage}/>
        <Route exact path="/pizza" component={PizzaPage}/>
      </Switch>
    </Router>
  ); 
};

export default App;
