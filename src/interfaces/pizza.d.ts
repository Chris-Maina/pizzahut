import { IUser } from './user';
/**
 * @interface pizza
 */
export interface IPizza {
  size?: string;
  name?: string;
  description?: string;
  price?: number;
}

export interface IPizzaPageProps {
  user?: IUser;
  samplePizza: IPizza;
}
