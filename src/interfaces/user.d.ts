import { REGISTER_USER_SUCCESS } from "../types/index";
/**
 * @interface IUser
 */
export interface IUser{
  username: string;
  email: string;
  password: string;
  createdAt: Date;
  modifiedAt: Date;
}

export interface IRegisterSuccess{
  user: IUser;
  type: REGISTER_USER_SUCCESS;
}