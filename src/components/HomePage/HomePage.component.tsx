import * as React from 'react';

// styles
import './HomePage.css';

// components
import { NavigationBar } from '../NavigationBar/NavigationBar.component';

export const HomePage = () => {
  return (
    <div>
      <NavigationBar />
      <section className="image-background">
        <div className="page-content">
          <h1>
            Sugar, spice and a large pizza slice!
          </h1>
        </div>
      </section>
    </div>
  );
};
