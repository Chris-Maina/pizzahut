// React lib import
import * as React from 'react';

// third-party libraries
import { Link } from 'react-router-dom';

// styles
import './NavigationBar.css';

export class NavigationBar extends React.Component<any, any> {
  constructor(props: any, context: any) {
    super(props, context);
    this.state = { scrollTop: false };
  }

  public componentDidMount() {
    window.addEventListener('scroll', () => {
      const navbarClass: HTMLElement = document.getElementsByClassName('navbar-wrapper')[0] as HTMLElement;
      navbarClass.className = window.scrollY > 10 ? 'navbar-wrapper black' : 'navbar-wrapper';
    });
  }

  public componentWillMount() {
    window.removeEventListener('scroll', () => undefined); 
  }

  public render() {
    let navClass = ['navbar-wrapper'];
    if (this.state.scrollTop) {
      navClass.push('black');
    }
    return (
      <nav>
        <div id="navbar" className="navbar-wrapper">
          <Link to="/" className="navbrand">LOGO</Link>
          <div className="navlinks-wrapper">
            <ul>
              <li>
                <Link to="#">
                  <a>Register</a>
                </Link>
              </li>
              <li>
                <Link to="#">
                  <a>log in</a>
                </Link>
              </li>
              <li>
                <Link to="/pizza">
                  <a className="active">Pizzas</a>
                </Link>
              </li>
              <li>
                <Link to="#">
                  <a>logout</a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}
