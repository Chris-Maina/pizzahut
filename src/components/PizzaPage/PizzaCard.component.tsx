// React lib import
import * as React from 'react';

// third-party libraries
import { Card } from 'semantic-ui-react';

// fixtures
import { PizzaButton } from '../../common/PizzaButton.compoment';

const button = (
  <PizzaButton
    color="blue"
    label="Buy"
  />
);
export const PizzaCard = ({samplePizza}: any): any => {
  return (
    <div>
      <Card
        header={samplePizza.name}
        meta={samplePizza.price}
        description={samplePizza.description}
        extra={button}
      />
    </div>
  );
};
