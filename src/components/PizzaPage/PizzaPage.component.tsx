// React lib import
import * as React from 'react';

// third-party libraries

// styles;

// components
import { NavigationBar } from '../NavigationBar/NavigationBar.component';
import { PizzaCard } from './PizzaCard.component';

// interfaces
import { IPizzaPageProps } from '../../interfaces/pizza';

// fixtures
import { samplePizza } from '../../fixtures/pizza';

class PizzaPage extends React.Component<IPizzaPageProps> {

  /**
   *  Renders the pizza page
   * @memberof IPizzaPageProps
   */
  public render() {
    return (
      <div>
        <NavigationBar />
        <PizzaCard
          samplePizza={ samplePizza }
        />
      </div>
    );
  }
}
export default PizzaPage;