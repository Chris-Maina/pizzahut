# PizzaHut

This is an client side application that consumes the PizzaHutAPI. The application helps you order and buy `pizza` from the PizzaHut store.

Technologies used: 

        React
        Typescript
        React-redux
        Semantic UI

## Getting started

You will need to have `npm` installed. In order to do this, install node using instructions [here](https://nodejs.org/en/download/). `npm` will be installed as part of node.

### Installing
* First, you need to clone the git repository 

    ``` $ git clone https://Chris-Maina@bitbucket.org/Chris-Maina/pizzahut.git```

* After you have cloned the repository into your local machine: 

    ```$ cd pizzahut```

* Now you are required to install all the modules for your project, this you will do by running this command on your cmd:
 NB: Make sure you have a package.json file on the root folder

    ```$ npm install```

* Finally use this command to run your application.

    ```$ npm start```

## Running the tests
This is how you will run the tests for the system:
    ```$ npm test```
